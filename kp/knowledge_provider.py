# -*- coding: utf-8 -*- 
import json
import logging
import pandas
import dateutil.parser
import sys

#python sys default decode method: ascii
#so change sys decode from ascii to utf-8 done ! No Error Fxck u
reload(sys)
sys.setdefaultencoding('utf-8')



class KnowledgeProvider:
	#request for per sentence
	request = {} 
	#store overall context intent
	dialogue_state = {'turns':0, 'request':''}
	filtered_posts=[]
	DATABASE_PATH =''
	DATABASE =[]


	def __init__(self,database_path):
		self.DATABASE_PATH = database_path

		try:
			with open(self.DATABASE_PATH) as f:
				self.DATABASE = json.load(f)
			logging.info('Database load successfully !')
		except:
			logging.warning('The database is not exist : ' + str(self.DATABASE_PATH))


	#Dialogue State Update
	def query(self,request):
		self.request = request
		self.filtered_posts = self.DATABASE

		#[之後改]dialogue state update
		if type(self.request) is str:
			if self.request == "不我要找版":
				#reset turns of dialogue
				self.dialogue_state = {'turns':1, 'request':'','function':  'request_board'}	
				res = self.request_board()
			else:
				res = self.inform()
			return res
		else:
			request_tmp = self.extract_slot(self.request)
			#Dialogue State Tracking
			self.dialogue_state.update(request_tmp)


		print ('[Dialogue State] :' + str(self.dialogue_state))
		print "self.dialogue_state['turns']" + str(object=self.dialogue_state['turns'])
		

		#Define Intent
		if self.dialogue_state['function'] == 'request_board':
			res = self.request_board()
		elif self.dialogue_state['function'] == 'request_post':
			res = self.request_post()
		else:
			res = self.intent_missed()

		self.dialogue_state['turns'] += 1
		return res

	#Default for chatbot doesn't know anything
	def intent_missed(self):
		request = self.extract_slot()
		print (" Intent Missed ") 
		print '[Request]:' + str(request)
		raw_input("不好意思 我聽不太懂您在說什麼, 請問你是要我 幫忙找版 或是 找文章呢?")		

	#Ask more question for more information
	def dialogue_policy(self):
		sys.stdout = sys.__stdout__
		if self.dialogue_state['function'] == 'request_post':
			if 'board' not in self.dialogue_state.keys():
					print''
					print "你想找關於什麼版的文章? ex: sex,Gossiping,NBA,joke,LoL "
					self.dialogue_state['request'] = 'board'


			elif 'title' not in self.dialogue_state.keys():
					print''
					print '請問你要搜索的關鍵字為何?'
					self.dialogue_state['request'] = 'title'


			elif 'push' not in self.dialogue_state.keys():
					print''
					print "推文數大於多少你才想看?"
					self.dialogue_state['request'] = 'push'


		elif self.dialogue_state['function'] == 'request_board':
			if 'title' not in self.dialogue_state.keys():
					print''
					print "請告訴我你想找關於什麼樣的版"
					self.dialogue_state['request'] = 'title'
		sys.stdout = sys.stderr


	#User tell chatbot the information
	def inform(self):
		if self.dialogue_state['request'] == 'board' :
			self.dialogue_state.update({'board': self.request})
		elif self.dialogue_state['request'] =='title':
			self.dialogue_state.update({'title': self.request})
		elif self.dialogue_state['request'] =='push':
			self.dialogue_state.update({'push':{'all':None,'score':int(self.request),'g':None,'b':None,'n':None}})
		else:
			print('inform : do nothing!')


	def reset(self):
		with open("kp_sf.json", "a") as myfile:
    			myfile.write(json.dumps(self.dialogue_state)+"\n")
		self.dialogue_state = {'turns':-1, 'request':''}


	# Find post via dialogue state
	def request_post(self):

		#To find the posts which are fit the dialogue state
		self.find_strategy(self.dialogue_state)

		#posts <=5 or dialogue management 做完了 就print 結果
		if len(self.filtered_posts) >=5  and 'push' not in self.dialogue_state.keys():
			self.dialogue_policy()
		else:
			sys.stdout = sys.__stdout__
			res = ('[Request]:' + str(self.dialogue_state) + '\n'
			     + '[Total posts]:' + str(len(self.DATABASE)) + '\n'
			     + '[Filtered posts]:' + str(len(self.filtered_posts)) +'\n\n')

			try:
				MAX_SHOW_POST = 5
				show_post_count = 0
				for post in self.filtered_posts:
					show_post_count += 1
					if show_post_count <= MAX_SHOW_POST:
						res += '(' + str(show_post_count) +')' 'Title->' + str(post['title']) + '\n'
						res += 'Link: ' + str(post['link'])
						# json.dumps({'(' + str(show_post_count) +')' 'Title->' + str(post['title'])})

			except:
				res += 'There is no posts for you idiot~'
			sys.stdout = sys.stderr

			#reset turns of dialogue
			self.reset()
		return res


	# Find board from dialogue state
	def request_board(self):
		print('request_board')

		#Dialogue Policy:
		self.find_strategy(self.dialogue_state)

		#有相應的文章但沒關鍵字
		if len(self.filtered_posts) >0  and 'title' not in self.dialogue_state.keys():
			self.dialogue_policy()
		else:
			sys.stdout = sys.__stdout__				
			try:
				res = 'The board you would like is:' + str(self.filtered_posts[0]['board'])
			except:
				res = 'There is no board fucking for yo u! Got it?'
			sys.stdout = sys.stderr

			#reset turns of dialogue
			self.reset()
		return res

	#Input string date , Ouput datetime.datetime 
	#Input   : 'Wed Apr  5 23:22:54 2017'
	#Output: datetime.datetime(2017, 4, 5, 23, 22, 54)
	def extract_date(self,date_string):
		return dateutil.parser.parse(date_string)

	def decode_utf8(self,string):
		return string.decode('utf-8').encode('utf-8')


	# filter post via dialogue state
	def find_strategy(self,request):
		# Strategy of finding posts
		for k,v in request.iteritems():

			if k == 'board':
				self.filtered_posts = filter(lambda posts: posts[k] == v, self.filtered_posts)			
			elif k == 'title':
				#find request keyword in database title
				self.filtered_posts = filter(lambda posts: self.decode_utf8(v) in self.decode_utf8(posts[k]), self.filtered_posts)
				a=0
			elif k == 'author':
				self.filtered_posts = filter(lambda posts: self.decode_utf8(v) in self.decode_utf8(posts[k]), self.filtered_posts)				

			elif k == 'content':
				a=0
			elif k == 'comment':
				a=0
			elif k == 'push':
				for key,val in request[k].iteritems():
					if key=='all':
						self.filtered_posts = filter(lambda posts: posts[k][key] >= val, self.filtered_posts)
					elif key=='score':
						self.filtered_posts = filter(lambda posts: posts[k][key] >= val, self.filtered_posts)
					elif key=='good':
						self.filtered_posts = filter(lambda posts: posts[k][key] >= val, self.filtered_posts)				
					elif key=='bad':
						self.filtered_posts = filter(lambda posts: posts[k][key] >= val, self.filtered_posts)
					elif key=='none':
						self.filtered_posts = filter(lambda posts: posts[k][key] >= val, self.filtered_posts)
					else:
						self.filtered_posts = filter(lambda posts: posts[k][key] >= val, self.filtered_posts)

			elif k == 'date':
				a=0
			elif k == 'ip':
				a=0
			else:
				a=0

	#remove user request which value is None, in other words, remain useful slot
	def extract_slot(self, request):		
		request_tmp = request

		for key, val in list(request_tmp.iteritems() ):
			if key == 'comment'  or key =='push':
				for k,v in list(request_tmp[key].iteritems()) :
					if v is None:
						del request_tmp[key][k]
				if request_tmp[key] =={}:
					del request_tmp[key]
			else:
				if val is None:
					del request_tmp[key]

		return request_tmp



# e.g Turn 爆 into 100
def keyword_translate(request, keyword_dict):
    req = request

    for key,val in keyword_dict.iteritems():
        if req['board'] == key:
            req['board'] = val

    for k,v in req['push'].iteritems():
        for key,val in keyword_dict.iteritems():
            if v == key:
                req['push'][k] = val
                print (val)
    return req




#request = {'function':None,'board':None,'title':None,'author':None,'content':None,'comment':{'state':None,'message':None,'id':None,'date':None},'push':{'all':None,'score':None,'good':None,'bad':None,'none':None},'date':None,'ip':None}



